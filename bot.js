const {readFileSync} = require('fs')
const moment = require('moment')
const axios = require('axios')
var _ = require('lodash')

const Telegraf = require('telegraf')
const TelegrafInlineMenu = require('telegraf-inline-menu')

async function startup() {
  let citiesDB = await axios.get("http://127.0.0.1:3333/cities/");
  let arrCities = citiesDB.data;
  let cities = _.map(arrCities, 'id');

  function wheatherResponse(ciudad,wheather){
let wheatherHtmlTemplate = 
`The weather in <b>${ciudad}</b> to the day <b>${moment(wheather.date,"YYYY-MM-DD").format("DD-MM-YYYY")}</b>:
🔹 <b><i>${wheather.phrase}</i></b>
Temperature: <b>${wheather.templow} - ${wheather.temphigh} °C</b>
Precip: <b>${wheather.precip} %</b>
`
return wheatherHtmlTemplate;
  }
  
  function personButtonText(_ctx, key) {
    let city = _.filter(arrCities, { 'id': parseInt(key) });
    return city[0]["name"]
  }
  function citySelectText(ctx) {
    let city = _.filter(arrCities, { 'id': parseInt(ctx.match[1]) });
    return `Select the day to see ${city[0]["name"]} forecast wheather.`
  }
  function getForecast(ctx){
    let day = moment();
    let dates_city_forecast = [day.format("DD-MM-YYYY")];
    for(let i=0; i<6; i++){
      dates_city_forecast.push(day.add(1,"d").format("DD-MM-YYYY"));
    }
    return dates_city_forecast;
  }
  const menu = new TelegrafInlineMenu('Select city to see the weather forecast!')
  const datesSelectSubmenu = new TelegrafInlineMenu(citySelectText)
  .select('f', getForecast, {
    setFunc: async (ctx, key) => {
     let city_id = ctx.match[1];
     let date = key;
     let city = _.filter(arrCities, { 'id': parseInt(city_id) });
     axios.get(`http://127.0.0.1:3333/cities/${city_id}/forecast/${date}`)
    .then((response)=>{
      let wheather = response.data[0];
      ctx.replyWithHTML(wheatherResponse(city[0]["name"],wheather), {parse_mode: "HTML"})
    })
    .catch((error)=>{
      ctx.reply("There is not regitry for the day selected")
    })
    },
    columns: 3
  })
  
  menu.selectSubmenu('p', cities, datesSelectSubmenu, {
    textFunc: personButtonText,
    columns: 2
  })
  
  menu.setCommand('start')
  
  const token = readFileSync('token.txt', 'utf8').trim()
  const bot = new Telegraf(token)
  
  bot.use(async (ctx, next) => {
    if (ctx.callbackQuery) {
      console.log('another callbackQuery happened', ctx.callbackQuery.data.length, ctx.callbackQuery.data)
    }
    return next()
  })
  
  bot.use(menu.init({
    mainMenuButtonText: 'back to main menu…',
  }))
  
  bot.catch(error => {
    console.log('telegraf error', error.response, error.parameters, error.on || error)
  })

  await bot.launch()
  console.log(new Date(), 'Bot started as', bot.options.username)
}

startup();